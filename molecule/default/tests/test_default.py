"""Role testing files using testinfra."""
import testinfra.utils.ansible_runner
import os
import pytest

pytest.testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
)

managers = pytest.testinfra_hosts.get_hosts('managers')

def test_traefik_docker_config(host):
    hostname = host.check_output('hostname')
    if hostname in managers:
        docker_config_info = host.check_output('docker config ls')
        assert 'traefik_config' in docker_config_info

